//Importing the needed packages
import java.text.DecimalFormat;
import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 * Title: Assignment1 
 * @author jjh20130- John Hambel - CSC200 MoWe 1:30
 * 
 * Summary:
 * This program is from chapter 2 number 9 on page 133. The objective is to add 
 * JOptionpane graphics to any other previous program in the chapter. I selected 
 * number 1, a program which needed to take input of a Fahrenheit number and convert
 * it to a Celcius number up to the first decimal point.
 *
 *How the program works:
 *The program runs by 3 methods: getInput(), convertFtoC(), and displayResult(). The 
 *program starts with getInput(), which just asks for an input in the form of a 
 *Fahrenheit number. The method then returns the parsed input(this program assumes
 *the user inputs a double). Then the program runs convertFtoC(), which takes in a 
 *double(assuming a Fahrenheit value) and converts it to a Celcius. Finally, the program
 *runs displayResult(), which intakes two doubles (one is the original Fahrenheit value 
 *and the other is the final Celcius value).
 *
 */

public class Assignment1 {
	
	//Declaring the values for original temperature (temp), the final temperature 
	//	(newTemp), the scanner for inputs, and a Decimal Format for rounding the fianl
	//		temp to one decimal place
	static double temp;
	static double newTemp;
	static Scanner scan= new Scanner(System.in);
	static DecimalFormat oneDigit = new DecimalFormat("#,##0.0");
	
	//Gets the input for the original temperature (in Fahrenheit)
	public static double getInput(){
		
		return Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter a "
				+ "temperature in Fahrenheit: ", JOptionPane.PLAIN_MESSAGE));
	}
	
	//Converts the original temperature to Celcius
	public static double convertFtoC(double x){
		
		return  5*(x-32)/9;
	}
	
	//Displays the original temperature and the resulting final temperature
	public static void displayResult(double temp1, double temp2){
		JOptionPane.showMessageDialog(null,temp + " degrees Fahrenheit is equal to " + 
				oneDigit.format(newTemp) + " degrees Celcius" );
	}
	
	//The main of the program
	public static void main(String[] args){
		
		temp= getInput();
		newTemp= convertFtoC(temp);
		displayResult(temp,newTemp);
	}
}
